require 'jekyll/spdx_licenses/spdx_filter'

RSpec.describe Jekyll::SpdxLicenses::SpdxFilter do
  include Jekyll::SpdxLicenses::SpdxFilter

  context '#spdx_link_id' do
    context 'when we have a valid license' do
      it 'returns the a link to the SPDX entry with the license ID as the link text' do
        expect(spdx_link_id('MIT')).to eq('<a href="https://spdx.org/licenses/MIT.html">MIT</a>')
      end
    end

    context 'when we do not have a valid license' do
      it 'raises an error' do
        expect { spdx_link_id('WibbleFoo') }.to raise_error('License ID not valid according to SPDX licenses')
      end
    end
  end

  context '#spdx_link_name' do
    context 'when we have a valid license' do
      it 'returns the a link to the SPDX entry with the license ID as the link text' do
        expect(spdx_link_name('MIT')).to eq('<a href="https://spdx.org/licenses/MIT.html">MIT License</a>')
      end
    end

    context 'when we do not have a valid license' do
      it 'raises an error' do
        expect { spdx_link_name('WibbleFoo') }.to raise_error('License ID not valid according to SPDX licenses')
      end
    end
  end

  context '#spdx_name' do
    context 'when we have a valid license' do
      it 'returns the full license name' do
        expect(spdx_name('Apache-2.0')).to eq('Apache License 2.0')
      end
    end

    context 'when we do not have a valid license' do
      it 'raises an error' do
        expect { spdx_name('RibbleRabble') }.to raise_error('License ID not valid according to SPDX licenses')
      end
    end
  end
end
