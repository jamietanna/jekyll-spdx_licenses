# Jekyll::SpdxLicenses

Liquid Tag to be used with Jekyll, which allows you to reference [SPDX](https://spdx.org/) licenses within your posts/projects.

To experiment with that code, run `bin/console` for an interactive prompt.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'jekyll-spdx_licenses'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install jekyll-spdx_licenses

## Usage

To convert a SPDX license ID to its full name:

```
{{ Apache-2.0 | spdx_name }}
=> Apache License 2.0
```

To convert the SPDX license ID to a link:

```
{{ Apache-2.0 | spdx_link_name }}
=> <a href="https://spdx.org/licenses/Apache-2.0">Apache License 2.0</a>
```

To convert the SPDX license ID to a link, with just the ID:

```
{{ Apache-2.0 | spdx_link_id }}
=> <a href="https://spdx.org/licenses/Apache-2.0">Apache-2.0</a>
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://gitlab.com/jamietanna/jekyll-spdx_licenses
