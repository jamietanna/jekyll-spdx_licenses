require 'liquid'
require 'jekyll/spdx_licenses/spdx_filter'
require "jekyll/spdx_licenses/version"

module Jekyll
  module SpdxLicenses
    class Error < StandardError; end
    # Your code goes here...
  end
end

Liquid::Template.register_filter(Jekyll::SpdxLicenses::SpdxFilter)
