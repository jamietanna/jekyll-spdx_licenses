require 'spdx-licenses'

module Jekyll
  module SpdxLicenses
    module SpdxFilter
      def spdx_link_id(license_id)
        license = ::SpdxLicenses.lookup(license_id)
        raise 'License ID not valid according to SPDX licenses' unless license
        "<a href=\"https://spdx.org/licenses/#{license.id}.html\">#{license.id}</a>"
      end

      def spdx_link_name(license_id)
        license = ::SpdxLicenses.lookup(license_id)
        raise 'License ID not valid according to SPDX licenses' unless license
        "<a href=\"https://spdx.org/licenses/#{license.id}.html\">#{license.name}</a>"
      end

      def spdx_name(license_id)
        license = ::SpdxLicenses.lookup(license_id)
        raise 'License ID not valid according to SPDX licenses' unless license
        license.name
      end
    end
  end
end
